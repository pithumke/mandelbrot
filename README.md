### What is this repository for? ###

* This repository contains an implementation that calculates a still image of the Mandelbrot set in order to demonstrate parallel processing using WebWorkers.

### How do I get set up? ###

* Checkout the repository
* Go to branch "demo"
* run ```npm install```
* run ```npm run watch```
* Go to "http://localhost:8080" or whatever other port the watch script is telling you

### Demo script ###

** Change the viewport **

  * By default, the image that gets calculated shows the area spanned between -2 + i and 1 - i.
  * To zoom to a more interesting image section, change the variable ```VIEWPORT_ZOOM``` in file ```Mandelbrot.worker.js``` to ```true```.
  * This makes the program calculate the area spanned between -0.625 + 0.5i and -0.525 + 0.45i

** Change the parallelism **

  * By default, the image gets calculated on only one worker thread to show the "regular" performance
  * To enable full parallel processing, change the variable ```LIMIT_CONCURRENCY``` in file ```website.js``` to ```false```.

### Who do I talk to? ###

* pit.humke@sap.com

### License ###

* UNLICENSED