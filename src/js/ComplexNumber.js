export class ComplexNumber
{
	constructor (real, imaginary)
	{
		if(isNaN(real)) {
			throw `${real} is not a number`
		}
		if(isNaN(imaginary)) {
			throw `${real} is not a number`
		}
		this._real = real;
		this._imaginary = imaginary;
	}

	getReal ()
	{
		return this._real;
	}
	getImaginary ()
	{
		return this._imaginary;
	}
	getMagnitude ()
	{
		let dMagnitude = Math.sqrt(
			Math.pow(this.getReal(), 2) + Math.pow(this.getImaginary(), 2)
		);
		if(isNaN(dMagnitude)) {
			return Infinity;
		} else {
			return dMagnitude;
		}
	}
	toString ()
	{
		return `${this.getReal()} + ${this.getImaginary()}i`;
	}
	plus (that)
	{
		return new ComplexNumber(
			this.getReal() + that.getReal(),
			this.getImaginary() + that.getImaginary()
		);
	}
	times (that)
	{
		if(that instanceof ComplexNumber === false) {
			that = new ComplexNumber(that, 0);
		}
		return new ComplexNumber(
			(this._real * that._real) - (this._imaginary * that._imaginary),
			(this._real * that._imaginary) + (this._imaginary * that._real)
		);
	}
	blowsUp ()
	{
		let z = new ComplexNumber(0, 0);
		let c = this;

		let oCurrentValue = (z.times(z)).plus(c);

		for(let i = 0; i <= 20; i++)
		{
			try
			{
				oCurrentValue = (oCurrentValue.times(oCurrentValue)).plus(c);
			}
			catch (e) {
				return true;
			}
		}

		if(oCurrentValue.getMagnitude() > 100) {
			return true;
		} else {
			return false;
		}
	}
	iterationsUntil (iTargetValue, iIterationLimit)
	{
		let z = new ComplexNumber(0, 0);
		let c = this;

		for(let i = 0; i <= iIterationLimit; i++)
		{
			if(z.getMagnitude() > iTargetValue) {
				return i;
			}
			z = (z.times(z)).plus(c);
		}
		//Could be big, small or looping
		return iIterationLimit;
	}
}