
import { ComplexNumber } from "./ComplexNumber";

let _oScreenSize;
let _oScreenPortion;
let oTopLeftBoundary;
let oBottomRightBoundary;

const VIEWPORT_ZOOM = false;
if (VIEWPORT_ZOOM) {
    oTopLeftBoundary = new ComplexNumber(-0.625, 0.5);
    oBottomRightBoundary = new ComplexNumber(-0.525, 0.45);
} else {
    oTopLeftBoundary = new ComplexNumber(-2, 1);
    oBottomRightBoundary = new ComplexNumber(1, -1);
}

function _initialize ()
{
    self.addEventListener("message", (event) => {
        switch(event.data.message) {
            case "SET_SCREEN_SIZE": {
                _setScreenSize({
                    height: event.data.data.height,
                    width: event.data.data.width
                });
                break;
            }
            case "SET_SCREEN_PORTION": {
                console.log("SET_SCREEN_PORTION", {
                    start: event.data.data.start,
                    end: event.data.data.end
                });
                _setScreenPortion({
                    start: event.data.data.start,
                    end: event.data.data.end
                });
                break;
            }
            case "CALCULATE_FRAME": {
                _calculateFrame();
                break;
            }
            default: {
                break;
            }
        }
    });
}
function _getScreenSize ()
{
	return _oScreenSize;
}
function _setScreenSize ({height, width} = {})
{
	_oScreenSize = {
		height,
		width
	}
}
function _getScreenPortion ()
{
    return _oScreenPortion;
}
function _setScreenPortion ({start, end} = {})
{
    _oScreenPortion = {
        start,
        end
    }
}
function _setRow (iRowIndex, aPixels)
{
	self.postMessage({
		message: "ROW_CALCULATED",
		data: {
			iRowIndex,
			aPixels
		}
	});
}
function _calculateFrame ()
{
    function percentage (min, max, value)
    {
        let normalizedMax = (max - min);
        let normalizedValue = (value - min);
        return normalizedValue / normalizedMax;
    }

    let dNormalizedX = oBottomRightBoundary.getReal() - oTopLeftBoundary.getReal();
    let dNormalizedY = oTopLeftBoundary.getImaginary() - oBottomRightBoundary.getImaginary();

    let oScreenSize = _getScreenSize();
    let oScreenPortion = _getScreenPortion();

    for(let y = oScreenPortion.start; y <= oScreenPortion.end; y++)
    {
    	let aPixels = [];
        for(let x = 0; x < oScreenSize.width; x++)
        {
            let dPercentX = percentage(0, oScreenSize.width, x);
            let dPercentY = percentage(0, oScreenSize.height, y);

            let oComplexNumber = new ComplexNumber(
                oBottomRightBoundary.getReal() - (dNormalizedX - (dNormalizedX * dPercentX)),
                ((dNormalizedY * dPercentY) - oTopLeftBoundary.getImaginary()) * -1 //Y axis is inverted
            );

            let iIterationsTo1000 = oComplexNumber.iterationsUntil(1000, 500);

        	aPixels.push({
        		x: x,
        		y: y,
        		r: iIterationsTo1000,
        		g: iIterationsTo1000,
        		b: iIterationsTo1000,
        		a: 255
        	});
        }
        _setRow(y, aPixels);
    }
}

_initialize();