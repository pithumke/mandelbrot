'use strict';

import MandelbrotWorker from "./Mandelbrot.worker.js"

let iNumWorkers;
const LIMIT_CONCURRENCY = true;
if (LIMIT_CONCURRENCY) {
    iNumWorkers = 1;
} else {
    iNumWorkers = navigator.hardwareConcurrency;
}

export class Website
{
    constructor ()
    {
        this._oCanvas;
        this._oContext;
        this._oImage;
        this._aWorkers;
    }
    start ()
    {
        this._startWork();
        window.requestAnimationFrame(this._drawFrame.bind(this));
    }
    onload ()
    {
        this._initializeCanvas();
        this._initializeViewport();
        this._initializeImage();
        this._initializeWorkers();
        this._updateScreenSize();

        this.start();
    }
    onresize ()
    {
        this._updateScreenSize();
        this._initializeImage();
    }
    _initializeCanvas ()
    {
        this._oCanvas = document.getElementById("canvas");
    }
    _initializeViewport ()
    {
        this._oContext = this._oCanvas.getContext("2d");
    }
    _initializeImage ()
    {
        let oScreenSize = this._getScreenSize();
        this._oImage = this._oContext.createImageData(oScreenSize.width, oScreenSize.height);
    }
    _initializeWorkers ()
    {
        this._aWorkers = [];
        for(let i = 0; i < iNumWorkers; i++)
        {
            let oWorker = new MandelbrotWorker();
            oWorker.addEventListener("message", (event) => {
                switch(event.data.message) {
                    case "ROW_CALCULATED": {
                        this._setRow(event.data.data.iRowIndex, event.data.data.aPixels);
                        break;
                    }
                    case "CALCULATION_DONE": {
                        break;
                    }
                    default: {
                        break;
                    }
                }
            });
            this._aWorkers.push(oWorker);
        }
    }
    _postAll (oMessage)
    {
        for(let i = 0; i < this._aWorkers.length; i++)
        {
            this._aWorkers[i].postMessage(oMessage);
        }
    }
    _updateScreenSize ()
    {
        let oScreenSize = this._getScreenSize();

        this._oCanvas.height = oScreenSize.height;
        this._oCanvas.width = oScreenSize.width;

        this._postAll({
            message: "SET_SCREEN_SIZE",
            data: {
                height: oScreenSize.height,
                width: oScreenSize.width
            }
        });

        let iWorkerHeight = Math.floor(oScreenSize.height / iNumWorkers);
        for(let i = 0; i < this._aWorkers.length; i++)
        {
            let iStart = i * iWorkerHeight;
            let iEnd = iStart + iWorkerHeight;
            if(iEnd > oScreenSize.height)
            {
                iEnd = oScreenSize.height
            }
            this._aWorkers[i].postMessage({
                message: "SET_SCREEN_PORTION",
                data: {
                    start: iStart,
                    end: iEnd
                }
            });
        }

    }
    _startWork ()
    {
        this._postAll({
            message: "CALCULATE_FRAME",
            data: null
        });
    }
    _setRow (iRowIndex, aPixels)
    {
        let oScreenSize = this._getScreenSize();
        for(let i = 0; i < aPixels.length; i++) {
            let iPosition = (iRowIndex * (oScreenSize.width * 4)) + (i * 4);
            this._oImage.data[iPosition + 0] = aPixels[i].r;
            this._oImage.data[iPosition + 1] = aPixels[i].g;
            this._oImage.data[iPosition + 2] = aPixels[i].b;
            this._oImage.data[iPosition + 3] = aPixels[i].a;
        }
    }
    _getScreenSize ()
    {
        return {
            height: document.documentElement.clientHeight,
            width: document.documentElement.clientWidth
        }
    }
    _drawFrame (iTimestamp)
    {
        this._oContext.putImageData(this._oImage, 0, 0);
        window.requestAnimationFrame(this._drawFrame.bind(this));
    }
}