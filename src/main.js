'use strict';

import { Website } from "./js/website";

export default function main ()
{

	//Create the Website object
	let oWebsite = new Website();

	//Run the initialization when the page gets loaded
	window.onload = () =>
	{
		oWebsite.onload()
	};

	//Resize the canvas when the window gets resized
	window.onresize = () =>
	{
		oWebsite.onresize()
	};
}