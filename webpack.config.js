var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var ROOT_PATH = path.resolve(__dirname);
var ENTRY_PATH = path.resolve(ROOT_PATH, 'src/include.js');
var SRC_PATH = path.resolve(ROOT_PATH, 'src');
var JS_PATH = path.resolve(ROOT_PATH, 'src/js');
var TEMPLATE_PATH = path.resolve(ROOT_PATH, 'src/index.html');
var BUILD_PATH = path.resolve(ROOT_PATH, 'target');
var NODE_MODULES = path.resolve(ROOT_PATH, 'node_modules');

var debug = process.env.NODE_ENV !== 'production';

module.exports = {
    entry: ENTRY_PATH,
    output: {
        path: BUILD_PATH,
        filename: 'bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Pit Humke | Under Construction',
            template: TEMPLATE_PATH,
            inject: 'body'
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: [
                    SRC_PATH
                ],
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015']
                }
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.worker.js$/,
                loader: "worker-loader"
            }
        ]
    },
    devtool: 'source-map'
};